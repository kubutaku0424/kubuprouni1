﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bombCont : MonoBehaviour
{
    public float dropSpeed;//爆弾の落ちるスピード

   public GameObject Explosion1;//爆発エフェクトを呼び出す
    float count = 0.0f;//爆弾が爆発するまでのカウント 
 
    //爆弾のそれぞれの座標
    float bomb_x;
    float bomb_y;
    float bomb_z;
    // Start is called before the first frame update
    void Start()
    {
    
    }

    // Update is called once per frame
    void Update()
    {
   //爆弾が落ちる処理
        transform.Translate(0, dropSpeed, 0);

        if (transform.position.y <= 0)//爆弾が地面に着いたら
        {
            count += Time.deltaTime;//カウント開始

            dropSpeed = 0;//それ以上落ちないようにする

            bomb_x =transform.position.x;//爆発する場所を指定
            bomb_y = transform.position.y;
            bomb_z =transform.position.z;

            if (count >1.0f)
            {
                GameObject explosion = Instantiate(Explosion1) as GameObject;
                explosion.transform.position = new Vector3(bomb_x, bomb_y, bomb_z);//爆発する場所を処理
                Destroy(explosion,1.0f);//爆発エフェクトを消す
                Destroy(gameObject);//爆弾も消す
                
                count = 0;
             
            }


        }

    }
}