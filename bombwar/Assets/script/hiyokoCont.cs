﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class hiyokoCont : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {//爆弾に当たるとほかのplayerの勝利の処理をします
        SceneManager.LoadScene("ClearScene1");
        Debug.Log("死す");
        if (other.gameObject.tag == "niwatori")
        {
            Debug.Log("Tag=niwatori");
        }
    }

    GameObject player1;//player1の位置情報を参照するため


    AudioSource audioSource;

    public GameObject cameraObject;

    public AudioClip hiyokowalkSE;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = cameraObject.GetComponent<AudioSource>();//AudioSourceから持ってくる 

        this.player1 = GameObject.Find("chikin");
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))//Aが押されたとき
        {

            if (transform.position.x == -1)//自機が左にいたら移動できない
            {

            }
            else
            {
                if (transform.position.z != this.player1.transform.position.z)//ほかのplayerと重ならない時に移動する(Z時)
                {
                    audioSource.PlayOneShot(hiyokowalkSE);//動けるときは歩行SEを鳴らす

                    Debug.Log("2");
                    transform.Translate(-1, 0, 0);

                }
                else
                {
                    if (transform.position.x - 1 == this.player1.transform.position.x)//ほかのplayerが先にその場にいれば移動できない
                    {
                  
                        Debug.Log("3");

                    }
                    else
                    {//それ以外は移動
                        audioSource.PlayOneShot(hiyokowalkSE);
                        Debug.Log("4");
                        transform.Translate(-1, 0, 0);
                    }
                }
                Debug.Log("1");
            }

        }


        if (Input.GetKeyDown(KeyCode.D))//Dが押されたとき
        {
            if (transform.position.x == 1)//自機が右にいたときは移動できない
            {

            }
            else
            {

                if (transform.position.z != this.player1.transform.position.z)//ほかのplayerと重ならない時に移動する(Z時)
                {
                    audioSource.PlayOneShot(hiyokowalkSE);

                    Debug.Log("2");
                    transform.Translate(1, 0, 0);

                }
                else
                {
                    if (transform.position.x + 1 == this.player1.transform.position.x)//ほかのplayerと重ならない時に移動する(X時)
                    {
                        Debug.Log("3");

                    }
                    else
                    {//それ以外は移動
                        audioSource.PlayOneShot(hiyokowalkSE);

                        Debug.Log("4");
                        transform.Translate(1, 0, 0);

                    }
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.W))//Wが押されたとき
        {
            if (transform.position.z == 1)//自機がに上いたら移動できない
            {

            }
            else
            {
                if (transform.position.x != this.player1.transform.position.x)//ほかのplayerと重ならない時に移動する(X時)
                {
                    audioSource.PlayOneShot(hiyokowalkSE);

                    Debug.Log("2");
                    transform.Translate(0, 0, 1);

                }
                else
                {
                    if (transform.position.z + 1 == this.player1.transform.position.z)//ほかのplayerと重ならない時に移動する(Z時)
                    {
                        Debug.Log("3");

                    }
                    else
                    {//それ以外は移動
                        audioSource.PlayOneShot(hiyokowalkSE);

                        Debug.Log("4");
                        transform.Translate(0, 0, 1);

                    }
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.S))//Sが押されたとき
        {
            if (transform.position.z == -1)//自機が下にいたら移動できない
            {

            }
            else
            {
                if (transform.position.x != this.player1.transform.position.x)//ほかのplayerと重ならない時に移動する(X時)
                {
                    audioSource.PlayOneShot(hiyokowalkSE);

                    Debug.Log("2");
                    transform.Translate(0, 0, -1);

                }
                else
                {
                    if (transform.position.z - 1 == this.player1.transform.position.z)//ほかのplayerと重ならない時に移動する(Z時)
                    {
                        Debug.Log("3");

                    }
                    else
                    {//それ以外は移動
                        audioSource.PlayOneShot(hiyokowalkSE);

                        Debug.Log("4");
                        transform.Translate(0, 0,-1);

                    }
                }
            }
        }


    }

}
