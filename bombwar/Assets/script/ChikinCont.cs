﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ChikinCont : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        SceneManager.LoadScene("ClearScene");
        Debug.Log("死す");
    }

    GameObject player2;//ほかのplayerのpositionを参照する

    int tx = -1;//最初の位置
    int tz = -1;

    AudioSource audioSource;

    public GameObject cameraObject;

    public AudioClip ChikinwalkSE;


    // Start is called before the first frame update
    void Start()
    {
        audioSource = cameraObject.GetComponent<AudioSource>();//AudioSourceから持ってくる 

        this.player2 = GameObject.Find("hiyoko");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))//左が押されたら
        {

            if (transform.position.x == -1)//自機が左にいたら移動できない
            {

            }
            else
            {
                if (transform.position.z != this.player2.transform.position.z)//ほかのplayerと重ならない時に移動する(Z時)
                {
                    audioSource.PlayOneShot(ChikinwalkSE);

                    Debug.Log("2");
                    transform.Translate(tx, 0, 0);

                }
                else
                {
                    if (transform.position.x - 1 == this.player2.transform.position.x)//ほかのplayerと重ならない時に移動する(X時)
                    {
                        Debug.Log("3");

                    }
                    else
                    {//それ以外は移動できる
                        audioSource.PlayOneShot(ChikinwalkSE);

                        Debug.Log("4");
                        transform.Translate(tx, 0, 0);

                    }
                }

                Debug.Log("1");
            }

        }
        

        if (Input.GetKeyDown(KeyCode.RightArrow))//右が押されたら
        {
            if (transform.position.x ==1)//自機が右にいたら移動できない
            {

            }
            else
            {
                if (transform.position.z != this.player2.transform.position.z)//ほかのplayerと重ならない時に移動する(Z時)
                {
                    audioSource.PlayOneShot(ChikinwalkSE);

                    Debug.Log("2");
                    transform.Translate(1, 0, 0);

                }
                else
                {
                    if (transform.position.x + 1 == this.player2.transform.position.x)//ほかのplayerと重ならない時に移動する(X時)
                    {
                        Debug.Log("3");

                    }
                    else
                    {//それ以外は移動できる 
                        audioSource.PlayOneShot(ChikinwalkSE);

                        Debug.Log("4");
                        transform.Translate(1, 0, 0);

                    }
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.UpArrow))//上が押されたら
        {
            if (transform.position.z == 1)//自機が上にいたら移動できない
            {

            }
            else
            {
                if (transform.position.x != this.player2.transform.position.x)//ほかのplayerと重ならない時に移動する(X時)
                {
                    audioSource.PlayOneShot(ChikinwalkSE);

                    Debug.Log("2");
                    transform.Translate(0, 0, 1);

                }
                else
                {
                    if (transform.position.z+ 1 == this.player2.transform.position.z)//ほかのplayerと重ならない時に移動する(Z時)
                    {
                        Debug.Log("3");

                    }
                    else
                    {//それ以外は移動できる 
                        audioSource.PlayOneShot(ChikinwalkSE);

                        Debug.Log("4");
                        transform.Translate(0, 0, 1);

                    }
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))//下が押されたら
        {
            if (transform.position.z ==-1)//自機が下にいたら移動できない
            {

            }
            else
            {
                if (transform.position.x != this.player2.transform.position.x)//ほかのplayerと重ならない時に移動する(X時)
                {
                    audioSource.PlayOneShot(ChikinwalkSE);

                    Debug.Log("2");
                    transform.Translate(0, 0, tz);

                }
                else
                {
                    if (transform.position.z - 1 == this.player2.transform.position.z)//ほかのplayerと重ならない時に移動する(Z時)
                    {
                        Debug.Log("3");

                    }
                    else
                    {//それ以外は移動できる
                        audioSource.PlayOneShot(ChikinwalkSE);

                        Debug.Log("4");
                        transform.Translate(0, 0, tz);

                    }
                }
            }
        }


    }
}
   
