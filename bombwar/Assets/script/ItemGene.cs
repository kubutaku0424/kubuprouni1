﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
public class ItemGene : MonoBehaviour
{
    //爆弾を呼び出す
    public GameObject bombPrefab;//爆弾のプレハブを参照
    float span = 2.0f;//スパン
    float delta = 0.0f;
    public float count_time = 0.0f;//カウント用変数


    public GameObject score_object = null; // Textオブジェクト

    void Start()
    {
   
    }

    void Update()
    {
        Text score_text = score_object.GetComponent<Text>();//テキスト表示用

        score_text.text = count_time.ToString("N0")+"秒経過";//描画します
        count_time += Time.deltaTime;//カウントをプラスしていきます

        this.delta += Time.deltaTime;
        if (count_time <= 10)//カウントが10より小さい時
        {
            if (this.delta > this.span)
            {
                this.delta = 0;

                GameObject item = Instantiate(bombPrefab) as GameObject;
                float x = Random.Range(-1, 2);//ランダムに場所を指定
                float z = Random.Range(-1, 2);
                item.transform.position = new Vector3(x, 4, z);//どのくらいから落とすか
            }
        }
        if (count_time > 10 && count_time <= 50)//カウントが10より大きい時かつ50より小さい時
        {
            if (this.delta > this.span)
            {
                span = 1.0f;
                this.delta = 0;

                GameObject item = Instantiate(bombPrefab) as GameObject;
                float x = Random.Range(-1, 2);//ランダムに場所を指定
                float z = Random.Range(-1, 2);
                item.transform.position = new Vector3(x, 4, z);//どのくらいから落とすか
            }

        }
        if (count_time > 50)//カウントが50より大きい時
        {
            if (this.delta > this.span)
            {
                span = 0.8f;
                this.delta = 0;

                GameObject item = Instantiate(bombPrefab) as GameObject;
                float x = Random.Range(-1, 2);//ランダムに場所を指定
                float z = Random.Range(-1, 2);
                item.transform.position = new Vector3(x, 4, z);//どのくらいから落とすか
            }
        }

        


    }


}
